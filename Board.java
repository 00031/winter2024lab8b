import java.util.Random;

public class Board {
	public Tile [][] grid;
	private final int size = 5;
// constructor
	public Board () {
		Random rng = new Random();
		this.grid = new Tile [size][size];
		for ( int i =0; i< this.grid.length; i++) {
			int randIndex = rng.nextInt(this.grid[i].length);
			for ( int j =0; j< this.grid[i].length; j++){
				if (j == randIndex) {
			   this.grid[i][j] = Tile.HIDDEN_WALL;
			}
			this.grid[i][j] = Tile.Blank;
			
		}
	}
	}
// toString method that creates the board 
public String toString (){
	String output ="";
	for (int i=0; i< this.grid.length; i++){
		output += i + " ";
		for (int j=0; j< this.grid[i].length; j++){
			output += this.grid[i][j].getName() + "  " ;
		}
		output += "\n";
	}
	return output;
 }
 // instance method that places the token 
public int placeToken(int row, int col) {
if (this.grid.length <= row || row < 0 || col > this.grid[0].length ){
		return -2;
	} else if (this.grid[row][col].equals(Tile.CASTLE) || this.grid[row][col].equals(Tile.WALL) ) {
		return -1;
	} else if ( this.grid[row][col].equals(Tile.HIDDEN_WALL)){
		this.grid[row][col] = Tile.WALL;
		return 1;
	} else {
		this.grid[row][col] = Tile.CASTLE;
		return 0;
 }
}
}

