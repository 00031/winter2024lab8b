public enum Tile {
	Blank("_"), 
	WALL("W"),
	HIDDEN_WALL("_"),
    CASTLE("C");
//fields
private final String name;


//constructor
private Tile(final String name) {
	this.name = name;
}

// get method
public String getName() {
	return this.name;
	}
}


